@echo off
setlocal enabledelayedexpansion

:: Define the date and time format for the tag (e.g., DD-MMM-YY HH.mm)
for /f "tokens=1-3 delims=/ " %%a in ('date /t') do (
    set "day=%%b"
    set "month=%%a"
    set "year=%%c"
)

:: Get the current time in HH.mm format
for /f "tokens=1-2 delims=: " %%a in ('time /t') do (
    set "hour=%%a"
    set "minute=%%b"
)

:: Create a tag with the current date and time
set "tag=%day%-%month%-%year% %hour%.%minute%"

:: Add all files
git add --all

:: Commit with a message containing the current date and time
git commit -m "Update - %tag%"

set "tag=%day%%month%%year%-%hour%-%minute%"

:: Creating a new branch with the tag name
git branch "%tag%"

:: Switch to the new branch
git checkout "%tag%"

:: Push the new branch to the remote repository
git push origin "%tag%"

:: Switch back to the original branch (assuming it's named 'main' - replace with your actual branch name)
git checkout master

:: Push to the current branch with the tag
git push origin HEAD



:: Cleanup
endlocal
